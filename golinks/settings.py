""" Settings that you can use to override the default_settings.py """
from golinks import default_settings

locals().update(default_settings.__dict__)

SQLALCHEMY_DATABASE_URI = 'sqlite://'
SECRET_KEY = 'CHANGE_ME'
