from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name="go-links",
    version="0.0.1",
    description="Go-Links",
    long_description=long_description,
    url="http://gitlab.com/danielawrence/go-links",
    license="MIT",
    packages=find_packages(exclude=['tests'])
)
